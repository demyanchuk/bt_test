var src_dir = __dirname+'/wp-content/themes/mogul/assets/src/',
    dist_dir = __dirname+'/wp-content/themes/mogul/assets/dist/',
    gulp = require('gulp'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename');

gulp.task('default', ['watch', 'sass']);

// Watch Task
gulp.task('watch', ['compress-js', 'minify-css', 'imagemin'], function(){
    livereload.listen();
    gulp.watch(src_dir+'js/*.js').on('change', livereload.changed);
    gulp.watch(src_dir+'sass/*.sass', ['sass']).on('change', livereload.changed);
    
});

// Compile sass files
gulp.task('sass', function () {
    return gulp.src(src_dir+'sass/main.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(src_dir+'css/'));
});

//Compress JS files
gulp.task('compress-js', function () {
    return gulp.src(src_dir+'js/*.js')
            .pipe(rename({suffix: '.min'}))
            .pipe(uglify())
            .pipe(gulp.dest(dist_dir+'js/'));
});

//Compress CSS files
gulp.task('minify-css', function () {
    return gulp.src(src_dir+'css/*.css')
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS())
        /*.pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))*/
        .pipe(gulp.dest(dist_dir+'css/'));
});

//Compress image files
gulp.task('imagemin', function () {
    return gulp.src(src_dir+'images/*')
        .pipe(imagemin())
        .pipe(gulp.dest(dist_dir+'images/'));
});
