<?php
$classesPath 	=  get_template_directory() . '/classes/';
require_once ($classesPath . 'functions_class.php');
require_once ($classesPath . 'products-post-type.php');
require_once ($classesPath . 'products-meta-box.php');
require_once ($classesPath . 'products.php');

ProductsPostType::getInstance();
ProductsMetaBox::getInstance();

$product1 = new Products(256);
//echo $product1->getPrice();
//echo $product1->isInStrock();
//$product1->getAdditionalImagesIds();
//$product1->the_content();
FunctionsClass::getInstance();
//add_image_size( 'team-member-image', 390, 325, array('center', 'top') );

/*function register_my_menu() {
  register_nav_menu('primary',__( 'Primary Menu' ));
}
add_action( 'init', 'register_my_menu' );*/

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}
?>