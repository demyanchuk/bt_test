<?php
/*
 * Template Name: Portfolio Page
 */
get_header();
?>
    <section class="content">
        <h1 class="content__title content__title-super-big text-center"><?php the_title(); ?></h1>
        <div class="content__navigation">
            <ul class="content__navigation-list">
                <?php
                $terms = get_terms( array(
                    'taxonomy' => 'portfolio_category',
                    'hide_empty' => false
                ) );
                foreach($terms as $term):
                ?>
                    <li class="content__navigation-list-item">
                        <a data-type="portfolio" data-taxonomy="portfolio_category" data-category-slug="<?php echo $term->slug; ?>" class="content__navigation-link js-category-link" href="javascript: void(0);"><?php echo $term->name; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="content__gallery js-items">
            <div class="content__gallery-wrapper js-category-items clearfix text-center">
                <?php

                $args = array(
                        'post_type' => 'portfolio_pt',
                        'tax_query' => array(
                                array(
                                        'taxonomy' => 'portfolio_category',
                                        'field' => 'slug',
                                        'terms' => 'beauty'
                                )
                        )
                    );

                $the_query = new WP_Query( $args );
                if($the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        get_template_part( 'partials/portfolio', 'loop' );
                    endwhile;
                endif; ?>
            </div>
        </div>
        <div class="content__description-block">
            <?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post(); ?>
                    <div class="content__description"><?php the_content(); ?></div>
                <?php endwhile;
            endif;
            ?>
        </div>
    </section>
<?php get_footer();
