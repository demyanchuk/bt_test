<?php
/*
 * Template Name: Reviews Page
 */
get_header();
?>
    <section class="content">
        <div class="content__page-container">
            <h1 class="content__title content__title-big text-center"><?php the_title(); ?></h1>
            <div class="reviews js-reviews clearfix" data-ppp="6">
                <?php

                $args = array(
                    'post_type' => 'reviews_pt',
                    'posts_per_page' => 6
                );

                $the_query = new WP_Query( $args );
                if($the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        get_template_part( 'partials/reviews', 'loop' );
                    endwhile;
                endif;
                wp_reset_query();
                ?>
            </div>
            <div class="content__more text-center"><a href="javascript: void(0);" class="content__more-link js-load-reviews">Load more</a></div>
            <div class="leave-review text-center">
                <a class="leave-review__link" href="<?php the_field('review_form_link', get_the_ID()); ?>"></a>
            </div>
            <div class="review-sites">
                <div class="review-sites__title">
                    <span class="review-sites__title_text"><?php the_field('review_sites_title'); ?></span>
                </div>
                <div class="review-sites__block">
                    <?php if( have_rows('review_sites') ):
                        $i = 0; while ( have_rows('review_sites') ) : the_row(); ?>
                        <a href="javascript: void(0);" class="review-sites__link js-review-site" data-id="<?php echo $i; ?>" data-post-id="<?php the_ID(); ?>">
                            <img class="review-sites__logo" src="<?php the_sub_field('review_site_logo'); ?>">
                        </a>
                        <?php $i++; endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();
