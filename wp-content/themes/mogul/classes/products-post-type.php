<?php
class ProductsPostType{
    private static $instance = null;

    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct(){
        add_action('init', [$this, 'products_init']);
    }
    public function products_init(){
        $labels = array(
            'name' => 'Products',
            'singular_name' => 'Product',
            'add_new' => 'Add new product',
            'add_new_item' => 'Add new product',
            'edit_item' => 'Edit product',
            'new_item' => 'New product',
            'view_item' => 'View product',
            'search_items' => 'Search products',
            'not_found' =>  'No products found',
            'not_found_in_trash' => 'No products found in trash',
            'parent_item_colon' => ''
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('with_front' => false),
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-list-view',
            'supports' => array('title','editor','thumbnail','custom-fields')
        );
        register_post_type('products',$args);

        register_taxonomy( 'product_category',
            'products',
            array(
                'hierarchical' => true,
                'label' => 'Product categories',
                'singular_label' => 'Product category',
                'public' => true,
                'show_tagcloud' => false,
                'query_var' => 'true',
                'show_ui' => true,
                'rewrite' => array('with_front' => false)
            )
        );
    }
}

