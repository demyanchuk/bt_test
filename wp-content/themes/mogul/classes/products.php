<?php
class Products{
    protected $wp_post;
    public function __construct($post){
        $this->post_id = $post;
        $this->wp_post = WP_Post::get_instance($post);
    }

    public function the_content(){
        echo $this->wp_post->post_content;
    }

    public function get_the_ID(){
        return $this->wp_post->ID;
    }

    public function getPrice(){
        $price = get_post_meta( $this->post_id, 'pr_settings_price');
        return (float) $price[0];
    }

    public function isInStrock(){
        $stock = get_post_meta( $this->post_id, 'pr_settings_stock');
        return $stock[0] ? $result = 'Product in stock' : 'Product out of stock';
    }

    public function getAdditionalImagesIds(){
        $attachments = get_post_meta( $this->post_id, 'pr_settings_gallery');
        $attachments = explode(',', $attachments[0]);
        return $attachments;
    }
}