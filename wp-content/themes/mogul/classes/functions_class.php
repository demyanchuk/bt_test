<?php

final class FunctionsClass{
    private static $instance = null;
    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct(){
        add_theme_support( 'post-thumbnails' );
        add_image_size( 'portfolio-image', 280, 420, array('center', 'top') );
        add_action( 'after_setup_theme', [FunctionsClass::class, 'woocommerce_support'] );
        add_action( 'wp_enqueue_scripts', [FunctionsClass::class, 'register_scripts'] );
        add_action( 'wp_ajax_nopriv_ajax_pagination', [FunctionsClass::class, 'my_ajax_pagination'] );
        add_action( 'wp_ajax_ajax_pagination', [FunctionsClass::class, 'my_ajax_pagination'] );
        add_action( 'wp_ajax_nopriv_posts_ajax_loading', [FunctionsClass::class, 'posts_ajax_loading'] );
        add_action( 'wp_ajax_posts_ajax_loading', [FunctionsClass::class, 'posts_ajax_loading'] );
        add_action( 'wp_ajax_newsletter_subscription', [FunctionsClass::class, 'newsletter_subscription'] );
        add_action( 'wp_ajax_newsletter_subscription', [FunctionsClass::class, 'newsletter_subscription'] );
        add_action( 'wp_ajax_nopriv_review_logo_description_loading', [FunctionsClass::class, 'review_logo_description_loading'] );
        add_action( 'wp_ajax_review_logo_description_loading', [FunctionsClass::class, 'review_logo_description_loading'] );
        add_action( 'widgets_init', [FunctionsClass::class, 'widgets_init'] );
    }

    public static function widgets_init() {
        register_sidebar( array(
            'name'          => 'Primary Sidebar',
            'id'            => 'sidebar-1',
            'description'   => 'Add widgets here to appear in your sidebar on blog posts and archive pages.',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>'
        ) );
    }

    public static function woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }

    public static function register_scripts() {
        wp_enqueue_style( 'magnific-popup', get_theme_file_uri( '/assets/dist/css/magnific-popup.min.css' ), array(), null );
        wp_enqueue_style( 'main', get_theme_file_uri( '/assets/dist/css/main.min.css'), array(), null );

        wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/ed3d884c2e.js', array(), null, true);
        wp_enqueue_script( 'magnific-popup', get_theme_file_uri( '/assets/dist/js/jquery.magnific-popup.min.js' ), array(), null, true);
        wp_enqueue_script( 'main', get_theme_file_uri( '/assets/dist/js/main.min.js'), array(), null, true);

        wp_localize_script( 'main', 'ajaxpagination', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        ));
    }

    public static function my_ajax_pagination() {
        $args = array(
            'post_type' => $_POST['type'].'_pt',
            'tax_query' => array(
                array(
                    'taxonomy' => $_POST['taxonomy'],
                    'field' => 'slug',
                    'terms' => $_POST['slug']
                )
            )
        );
        $posts = new WP_Query( $args );

        if( ! $posts->have_posts() ) {
            get_template_part( 'partials/'.$_POST['type'], 'empty' );
        }
        else {
            while ( $posts->have_posts() ) {
                $posts->the_post();
                get_template_part( 'partials/'.$_POST['type'], 'loop' );
            }
        }
        die();
    }

    public static function newsletter_subscription() {
        $email = $_POST['email'];
        $admin_email = get_option('admin_email');
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if(mail($admin_email, 'Newsletter subscription', 'Email: ' . $email)){
                echo '<div class="message-success">'.get_field('success_message', 'option').'</div>';
            } else {
                echo '<div class="message-error">'.get_field('error_message', 'option').'</div>';
            }
        }else{
            echo '<div class="message-error">'.get_field('error_message', 'option').'</div>';
        }
        die();
    }

    public static function posts_ajax_loading() {
        $args = array(
            'post_type' => 'reviews_pt',
            'posts_per_page' => $_POST['posts_per_page'],
            'offset' => $_POST['loaded_posts_count']
        );
        $posts = new WP_Query( $args );
        if( $posts->have_posts() ) {
            while ( $posts->have_posts() ) {
                $posts->the_post();
                get_template_part( 'partials/reviews', 'loop' );
            }
        }
        die();
    }

    public static function review_logo_description_loading() {
        $review_logos = get_field('review_sites', $_POST['post_id']);
        echo $review_logos[$_POST['id']]['review_site_description'];
        die();
    }

}