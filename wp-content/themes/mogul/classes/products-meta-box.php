<?php
class ProductsMetaBox{
    private static $instance = null;
    private $options = array(
        'id'	     =>	'pr_settings',
        'name'	     => 'Product settings',
        'post'	     =>	['products'],
        'position'   =>	'normal',
        'priority'   =>	'high',
        'capability' =>	'edit_posts',
        'args'	   =>	array(
            array(
                'id'			=>	'price',
                'title'			=>	'Price',
                'type'			=>	'text',
                'placeholder'	=>	'Product price',
                'desc'			=>	'',
                'capability'	=>	'edit_posts'
            ),
            array(
                'id'			=>	'stock',
                'title'			=>	'In stock',
                'type'			=>	'checkbox',
                'desc'			=>	'Product in stock',
                'capability'	=>	'edit_posts'
            ),
            array(
                'id'			=>	'gallery',
                'title'			=>	'Product additional images',
                'type'			=>	'gallery',
                'desc'			=>	'',
                'capability'	=>	'edit_posts'
            ),
        )
    );
    public static $prefix = 'pr_settings_';

    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    private function __construct(){
        add_action( 'admin_enqueue_scripts', [$this, 'admin_register_scripts'] );
        add_action( 'add_meta_boxes', [$this, 'create'] );
        add_action( 'save_post', [$this, 'save'], 1, 2 );
    }
    //Создаем мета бокс
    public function create() {
        foreach ($this->options['post'] as $post_type) {
            if (current_user_can( $this->options['capability'])) {
                add_meta_box($this->options['id'], $this->options['name'], array($this, 'fill'), $post_type, $this->options['position'], $this->options['priority']);
            }
        }
    }
    //Выводим нужные поля для мета бокса
    public function fill(){
        global $post;
        wp_nonce_field( $this->options['id'], $this->options['id'].'_wpnonce', false, true );
        $html = '<table class="form-table"><tbody>';
        foreach ( $this->options['args'] as $param ) {
            if (current_user_can( $param['capability'])) {
                $html .= '<tr>';
                if(!$value = get_post_meta($post->ID, $this->prefix .$param['id'] , true)) $value = $param['std'];
                switch ( $param['type'] ) {
                    case 'text':
                        $html .= '<th scope="row"><label for="'.$this->prefix .$param['id'].'">'.$param['title'].'</label></th>
                        <td>
                            <input name="'.$this->prefix .$param['id'].'" type="'.$param['type'].'" id="'.$this->prefix .$param['id'].'" value="'.$value.'" placeholder="'.$param['placeholder'].'" class="regular-text" /><br />
                            <span class="description">'.$param['desc'].'</span>
                        </td>';
                        break;

                    case 'checkbox':
                        $html .= '<th scope="row"><label for="'.$this->prefix .$param['id'].'">'.$param['title'].'</label></th>';
                        $html .= '<td>';
                            $checked = ($value=='on') ? ' checked="checked"' : '';
                        $html .= '   <label for="'.$this->prefix .$param['id'] .'"><input name="'.$this->prefix .$param['id'].'" type="'.$param['type'].'" id="'.$this->prefix .$param['id'].'" '.$checked.' />';
                                $html .= $param['desc'] .'</label>';
                        $html .= '</td>';
                        break;
                    case 'gallery':
                        $html .= '<th scope="row"><label for="'.$this->prefix .$param['id'].'">'.$param['title'].'</label></th>';
                        $html .= '<td>';
                        $html .= '  <ul class="product_images">';
                        if ( metadata_exists( 'post', $post->ID, $this->prefix .$param['id'] ) ) {
                            $product_image_gallery = get_post_meta( $post->ID, $this->prefix .$param['id'], true );
                        } else {
                            $attachment_ids = get_posts( 'post_parent=' . $post->ID . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids' );
                            $attachment_ids = array_diff( $attachment_ids, array( get_post_thumbnail_id() ) );
                            $product_image_gallery = implode( ',', $attachment_ids );
                        }

                        $attachments         = array_filter( explode( ',', $product_image_gallery ) );
                        $update_meta         = false;
                        $updated_gallery_ids = array();

                        if ( ! empty( $attachments ) ) {
                            foreach ( $attachments as $attachment_id ) {
                                $attachment = wp_get_attachment_image( $attachment_id, 'thumbnail' );

                                if ( empty( $attachment ) ) {
                                    $update_meta = true;
                                    continue;
                                }

                                $html .= '<li data-attachment_id="' . esc_attr( $attachment_id ) . '">' . $attachment ;
                                $html .= '<a href="#" class="remove-gallery-item">Remove</a></li>';

                                $updated_gallery_ids[] = $attachment_id;
                            }

                            // need to update product meta to set new gallery ids
                            if ( $update_meta ) {
                                update_post_meta( $post->ID, $this->prefix .$param['id'], implode( ',', $updated_gallery_ids ) );
                            }
                        }
                        $html .= '</ul>';

                        $html .= '<input type="hidden" id="product_additional_images" name="product_additional_images" value="'.esc_attr( $product_image_gallery ).'" />';

                        $html .= '<a class="add_product_images" href="/wp-admin/media-upload.php?post_id='.$post->ID.'&type=image&TB_iframe=1&width=753&height=185">Add product gallery images</a>';
                        $html .= '</td>';
                        break;
                }
                $html .= '</tr>';
            }
        }
        $html .= '</tbody></table>';

        echo $html;
    }
    //Сохраняем мета данные
    public function save($post_id, $post){
        if ( !wp_verify_nonce( $_POST[ $this->options['id'].'_wpnonce' ], $this->options['id'] ) ) return;
        if ( !current_user_can( 'edit_post', $post_id ) ) return;
        if ( !in_array($post->post_type, $this->options['post'])) return;
        foreach ( $this->options['args'] as $param ) {
            if ( current_user_can( $param['capability'] ) ) {
                if ( isset( $param['type'] ) && $param['type'] == 'gallery') {
                    $attachment_ids = isset( $_POST['product_additional_images'] ) ? array_filter( explode( ',', wc_clean( $_POST['product_additional_images'] ) ) ) : array();
                    update_post_meta( $post_id, $this->prefix . $param['id'], implode( ',', $attachment_ids ) );
                }elseif ( isset( $_POST[ $this->prefix . $param['id'] ] ) && trim( $_POST[ $this->prefix . $param['id'] ] ) ) {
                    update_post_meta($post_id, $this->prefix . $param['id'], trim($_POST[$this->prefix . $param['id']]));
                } else {
                    delete_post_meta( $post_id, $this->prefix . $param['id'] );
                }

            }

        }
    }

    public function admin_register_scripts() {
        wp_enqueue_script( 'metabox-custom', get_theme_file_uri( '/assets/js/admin.js' ), array(), null, true);
    }
}

