<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="header clearfix">
        <div class="header__mobile-menu"><i class="fa fa-bars fa-2x"></i></div>
        <?php $class = 'header__menu-pages clearfix';
        $class .= is_front_page() ? ' header__menu-pages-home' : '';
        wp_nav_menu( array(
            'container' => 'nav',
            'menu_class' => $class,
            'container_class' => "header__menu",
            'menu_id'    => 'menu1'
        ) );  ?>
        <a class="header__link button-link <?php echo is_front_page() ? 'header__link-home' : ''; ?>" href="<?php the_field('booking_link', 'option'); ?>"></a>
    </header>

    <section class="main-banner <?php echo is_front_page() ? 'main-banner-home' : ''; ?>" style="<?php if(get_the_post_thumbnail_url()): ?>background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium_large'); ?>)<?php endif; ?>">
        <?php if(get_the_post_thumbnail()):
                the_post_thumbnail('full', array('class'=>'img-responsive main-banner__image'));
              else:
                  $template_url = get_bloginfo('template_url');
                echo '<img src="'.$template_url.'/assets/dist/images/page-banner.jpg">';
              endif;
        ?>
    </section>
