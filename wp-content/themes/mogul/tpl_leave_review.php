<?php
/*
 * Template Name: Review Form Page
 */
acf_form_head();
get_header(); ?>

    <section class="content">
        <div class="content__page-wrapper text-center">
            <h1 class="content__title text-center"><?php the_title(); ?></h1>
            <?php

            if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post();
                    acf_form(array(
                        'post_id'		=> 'new_post',
                        'field_groups' => array( 367 ),
                        'label_placement' => 'left',
                        'new_post'		=> array(
                            'post_type'		=> 'reviews_pt',
                            'post_status'		=> 'draft'
                        ),
                        'submit_value'		=> 'Add review',
                        'updated_message' => __('Review was submitted', 'acf'),
                    ));
                endwhile;

            else :


            endif;
            ?>
        </div>
    </section>

<?php get_footer();
