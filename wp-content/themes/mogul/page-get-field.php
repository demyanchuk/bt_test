<?php
get_header();

isset($_GET['field']) ? $field = $_GET['field'] : $field = '';
isset($_GET['id']) ? $page = $_GET['id'] : $page = '';

if($field && $page):
    the_field($field, $page);
else:
    echo 'Недостаточно данных для вывода поля';
endif;

get_footer();