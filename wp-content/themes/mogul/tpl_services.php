<?php
/*
 * Template Name: Services Page
 */
get_header();
?>
    <section class="content">
        <h1 class="content__title text-center"><?php the_title(); ?></h1>
        <div class="content__navigation service-navigation has-lines">
            <ul class="content__navigation-list">
                <?php
                $terms = get_terms( array(
                    'taxonomy' => 'service_category',
                    'hide_empty' => false
                ) );
                foreach($terms as $term):
                    ?>
                    <li class="content__navigation-list-item">
                        <a data-type="services" data-taxonomy="service_category" data-category-slug="<?php echo $term->slug; ?>" class="content__navigation-link js-category-link" href="javascript: void(0);"><?php echo $term->name; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="content__services js-service-items">
            <?php $args = array(
                'post_type' => 'services_pt',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'service_category',
                        'field' => 'slug',
                        'terms' => 'bridal'
                    )
                )
            );

            $the_query = new WP_Query( $args );
            ?>
            <div class="content__services-wrapper js-category-items clearfix text-center">
                <?php
                if($the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        get_template_part( 'partials/services', 'loop' );
                    endwhile;
                endif; ?>
            </div>
            <div class="content__additional-services">
                <div class="service-additional__title text-center"><?php the_field('additional_services_title', 'option'); ?></div>
                <div class="content__additional-services-wrapper clearfix">
                    <?php
                    if($the_query->have_posts() ) : ?>

                    <?php  while ( $the_query->have_posts() ) : $the_query->the_post();
                            get_template_part( 'partials/additional-services', 'loop' );
                        endwhile;
                    endif; ?>
                </div>
            </div>
            <div class="content__additional-services">
                <div class="service-additional__title text-center"><?php the_field('travel_services_title', 'option'); ?></div>
                <div class="content__additional-services-wrapper clearfix">
                    <?php
                    if($the_query->have_posts() ) : ?>

                        <?php  while ( $the_query->have_posts() ) : $the_query->the_post();
                            get_template_part( 'partials/travel-services', 'loop' );
                        endwhile;
                    endif; ?>
                </div>
            </div>
        </div>

        <div class="content__service-examples">
            <div class="service-examples__title text-center"><?php the_field('service_examples_block_title', 'option'); ?></div>

            <?php if( have_rows('service_examples', 'option') ): ?>
                <ul class="service-examples__list clearfix">
            <?php    while ( have_rows('service_examples', 'option') ) : the_row(); ?>
                    <li class="service-examples__list-item"><?php the_sub_field('service_example_title'); ?></li>
            <?php    endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
    </section>
<?php get_footer();
