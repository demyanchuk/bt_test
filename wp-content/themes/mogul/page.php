<?php
get_header(); ?>

    <section class="content">
        <div class="content__page-wrapper content__page-wrapper-medium-margin  text-center">
            <h1 class="content__title content__title-big text-center"><?php the_title(); ?></h1>
            <?php
            if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile;

            else :


            endif;
            ?>
        </div>
    </section>

<?php get_footer();
