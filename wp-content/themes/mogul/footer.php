    <footer class="footer">
        <div class="footer__block">
            <div class="footer__title"><?php echo get_field('newsletter_title', 'option'); ?></div>
            <div class="footer__description"><?php echo get_field('newsletter_subtitle', 'option'); ?></div>
            <div class="footer__input-subscribe">
                <input type="text" class="footer__subscribe-input js__subscribe-item">
            </div>
        </div>
        <div class="footer__block footer__block-bigger">
            <div class="footer__title"><?php echo get_field('contacts_title', 'option'); ?></div>
            <div class="footer__description">
                <div class="footer__description-item"><?php echo get_field('owner_name', 'option'); ?></div>
                <div class="footer__description-item"><?php echo get_field('owner_positions', 'option'); ?></div>
            </div>
            <div class="footer__feedback-info">
                <div class="footer__address footer__contact-data"><i class="sprite sprite-map"></i><?php echo get_field('address', 'option'); ?></div>
                <div class="footer__email footer__contact-data"><i class="sprite sprite-email"></i><?php echo get_field('email', 'option'); ?></div>
                <div class="footer__phone footer__contact-data"><i class="sprite sprite-phone"></i><?php echo get_field('phone', 'option'); ?></div>
            </div>
        </div>
        <div class="footer__block footer__block-last">
            <div class="footer__title"><?php echo get_field('socials_title', 'option'); ?></div>
            <div class="footer__description"><?php echo get_field('socials_subtitle', 'option'); ?></div>
            <div class="footer__social-info clearfix">
                <?php
                while( have_rows('social_networks', 'option') ): the_row();
                    echo '<a class="footer__social-link" href="'.get_sub_field('social_link').'"><i class="sprite sprite-'.get_sub_field('social_icon_class').'"></i></a>';
                endwhile;
                ?>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>
