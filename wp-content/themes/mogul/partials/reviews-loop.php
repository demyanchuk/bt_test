<div class="reviews__review-item">
    <div>
        <span class="reviews__review-title"><?php the_field('review_upper_text'); ?></span><?php the_field('review_text'); ?>
    </div>
    <div class="reviews__review-author-info">
        <span class="reviews__author-name author-data">- <?php the_field('review_name'); ?></span>
        <span class="reviews__author-address author-data"><?php the_field('review_location'); ?></span>
    </div>
</div>