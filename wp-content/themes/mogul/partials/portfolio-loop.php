<div class="content__gallery-block">
    <a href="<?php the_post_thumbnail_url(); ?>" class="js-image">
        <?php the_post_thumbnail('portfolio-image', array('class' => 'content__gallery-block-image')); ?>
    </a>
</div>
