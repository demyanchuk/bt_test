<div class="blog__post_title text-center"><?php the_title(); ?></div>
<div class="blog__post_content"><?php the_content(); ?></div>