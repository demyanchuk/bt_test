<?php if(get_field('additional_option') || get_field('option_price')): ?>
<div class="content__additional-service-block">
    <div class="content__service-title"><?php the_field('additional_option'); ?></div>
    <div class="content__service-price"><?php the_field('option_price'); ?></div>
</div>
<?php endif; ?>