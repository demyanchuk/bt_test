<?php if(get_field('travel_option') || get_field('travel_price')): ?>
    <div class="content__additional-service-block">
        <div class="content__service-title"><?php the_field('travel_option'); ?></div>
        <div class="content__service-price"><?php the_field('travel_price'); ?></div>
    </div>
<?php endif; ?>