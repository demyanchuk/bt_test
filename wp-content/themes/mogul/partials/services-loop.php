<div class="content__service-block">
    <div class="content__service-title"><?php the_field('service_name'); ?></div>
    <div class="content__service-price"><?php the_field('service_price'); ?></div>
    <div class="content__service-description"><?php the_field('service_description'); ?></div>
</div>