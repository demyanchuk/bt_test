<?php 
/*
 * Template Name: Home Page
 */
get_header();
?>
<section class="content">
    <div class="content__home-container">
        <?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post(); ?>
                    <h1 class="content__title text-center"><?php the_field('content_title'); ?></h1>
                    <div class="content__article text-center"><?php the_content(); ?></div>
                    <div class="content__image-block">
                        <?php
                            $content_img = get_field('content_image');
                            if($content_img):
                                echo '<img class="content__article-img has-spaced-border" src="'.$content_img.'">';
                            endif;
                        ?>
                    </div>
                    <div class="content__article">
                        <?php the_field('content_part2'); ?>
                    </div>
                    <div class="content__author-description">
                        <div class="author-description__image">
                            <?php
                                $owner_photo = get_field('owner_photo');
                                if($owner_photo):
                                    echo '<img class="content__author-photo has-spaced-border " src="'.$owner_photo.'">';
                                endif;
                            ?>
                        </div>
                        <div class="content__author-name content__author-data"><?php the_field('author_name'); ?></div>
                        <div class="content__author-info content__author-data"><?php the_field('author_addr'); ?></div>
                        <div class="content__author-contacts">
                            <div class="content__author-email content__author-data"><?php the_field('author_email'); ?></div>
                            <div class="content__author-phone content__author-data"><?php the_field('author_phone'); ?></div>
                        </div>
                    </div>
                <?php endwhile;
            endif;
        ?>
    </div>
</section>
<?php get_footer();
