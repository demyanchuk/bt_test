<?php
/*
 * Template Name: Blog Page
 */
get_header();
?>
    <section class="content clearfix">
        <main class="content__left-block">
            <?php
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish'
            );

            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="blog__post">
                <?php    get_template_part( 'partials/blog', 'post' ); ?>
                    </div>
                <?php endwhile;
            endif;
            ?>
        </main>
        <aside class="content__sidebar">
            <?php dynamic_sidebar( 'sidebar-1' ); ?>
        </aside>
    </section>

<?php get_footer();
