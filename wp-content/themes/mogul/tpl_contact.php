<?php
/*
 * Template Name: Contacts Page
 */
get_header(); ?>

    <section class="content">
        <div class="content__page-wrapper">
            <h1 class="content__title content__title-big text-center"><?php the_title(); ?></h1>
            <?php
            if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile;

            else :


            endif;
        ?>
            <a class="content__review-link button-link js-button-link js-review-button-link" href="<?php the_permalink(get_field('review_link', 'option')); ?>"></a>
            <a class="content__appointment-link button-link js-button-link js-appointment-button-link" href="<?php the_permalink(get_field('appointment_link', 'option')); ?>"></a>
        </div>
    </section>

<?php get_footer();
