$ = jQuery;
$(document).ready(function(){
    // Product gallery file uploads.
    var product_gallery_frame;
    var $image_gallery_ids = $( '#product_additional_images' );
    var $product_images    = $( 'ul.product_images' );

    $( document ).on( 'click', '.add_product_images', function( e ) {
        var $el = $( this );

        e.preventDefault();

        if ( product_gallery_frame ) {
            product_gallery_frame.open();
            return;
        }

        // Create the media frame.
        product_gallery_frame = wp.media.frames.product_gallery = wp.media({
            // Set the title of the modal.
            title: $el.data( 'choose' ),
            button: {
                text: $el.data( 'update' )
            },
            states: [
                new wp.media.controller.Library({
                    title: $el.data( 'choose' ),
                    filterable: 'all',
                    multiple: true
                })
            ]
        });

        // When an image is selected, run a callback.
        product_gallery_frame.on( 'select', function() {
            var selection = product_gallery_frame.state().get( 'selection' );
            var attachment_ids = $image_gallery_ids.val();

            selection.map( function( attachment ) {
                attachment = attachment.toJSON();

                if ( attachment.id ) {
                    attachment_ids   = attachment_ids ? attachment_ids + ',' + attachment.id : attachment.id;
                    var attachment_image = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;

                    $product_images.append( '<li data-attachment_id="' + attachment.id + '"><img src="' + attachment_image + '" /><a href="#" class="remove-gallery-item">' + $el.data('text') + '</a></li>' );
                }
            });

            $image_gallery_ids.val( attachment_ids );
        });

        // Finally, open the modal.
        product_gallery_frame.open();
    });

    // Remove images.
    $( '.product_images' ).on( 'click', '.remove-gallery-item', function() {
        $( this ).closest( 'li' ).remove();

        var attachment_ids = '';

        $(  '.product_images li' ).each( function() {
            var attachment_id = $( this ).attr( 'data-attachment_id' );
            attachment_ids = attachment_ids + attachment_id + ',';
        });

        $image_gallery_ids.val( attachment_ids );

        return false;
    });
});

