(function($) {
    //Меню
    $(document).on('click', '.header__mobile-menu', function(){
        $('.header__menu-pages').toggle();
    });

    //Добавляем кнопки на странице контакты в блок с формой
    $('.js-button-link').appendTo('.wpcf7').css({'visibility': 'visible'});

    $(document).on('click', '.js-appointment-button-link', function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        var full_content = $('<div></div>');
        $.ajax({ type: "GET",
            url: href,
            success : function(html)
            {
                console.log(html)
                $(html).find('.content__page-wrapper').appendTo(full_content);
            }
        });
        $.magnificPopup.open({
            removalDelay: 350,
            mainClass: 'mfp-fade form-popup',
            items: {
                src: full_content
            },
            type: 'inline'
        }, 0);
    });

    if($(window).width() < 992) {
        //Submenu
        $(document).on('click', '.menu-item-has-children a', function (e) {
            e.preventDefault();
            $(this).parent().addClass('open');
            $('.menu-item-has-children.open > .submenu').show();
        });
    }

    $(document).on('click', '.menu-item-has-children.open a', function(){
        $(this).parent().removeClass('open');
        return true;
    });
    //Подписка на рассылку
    $(document).on('keydown', '.js__subscribe-item', function(e){
        if (e.which === 13) {
            e.preventDefault();
            var email = $(this).val();
            $.ajax({
                url: ajaxpagination.ajaxurl,
                type: 'post',
                data: {
                    action: 'newsletter_subscription',
                    email: email
                },
                success: function( response ) {
                    $('.footer__input-subscribe .message-success, .footer__input-subscribe .message-error').remove();
                    $(response).insertAfter('.js__subscribe-item');
                    setTimeout(function(){
                        $('.footer__input-subscribe .message-success, .footer__input-subscribe .message-error').remove();
                    }, 5000);
                }
            });
        }
    });
    //Открытие картинок в попапе
    $(document).on('click', '.js-image', function (e) {
        e.preventDefault();
        $.magnificPopup.open({
            removalDelay: 350,
            mainClass: 'mfp-fade image-popup',
            items: {
                src: $(this).attr('href')
            },
            type: 'image'
        }, 0);
    });

    //Вывод соответствующих постов портфолио или услуг при клике на ссылку категории
    $(document).on( 'click', '.js-category-link', function( e ) {
        e.preventDefault();
        var slug = $(this).data('category-slug');
        var type = $(this).data('type');
        var taxonomy = $(this).data('taxonomy');
        $.ajax({
            url: ajaxpagination.ajaxurl,
            type: 'post',
            data: {
                action: 'ajax_pagination',
                slug: slug,
                type: type,
                taxonomy: taxonomy
            },
            success: function( html ) {
                $( '.js-category-items' ).html('');
                $( '.js-category-items' ).append( html );
            }
        });
    });

    //Дозагрузка отзывов по клику на кнопку
    $(document).on('click', '.js-load-reviews', function(e){
        e.preventDefault();
        var posts_per_page = $('.js-reviews').data('ppp');
        var loaded_posts_count = $('.js-reviews').children().length;

        $.ajax({
            url: ajaxpagination.ajaxurl,
            type: 'post',
            data: {
                action: 'posts_ajax_loading',
                posts_per_page: posts_per_page,
                loaded_posts_count: loaded_posts_count
            },
            success: function( html ) {
                $( '.js-reviews' ).append( html );
            }
        });
    });

    //Подгрузки описания логотипа в отзывах
    $(document).on('click', '.js-review-site', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var post_id = $(this).data('post-id');
        $.ajax({
            url: ajaxpagination.ajaxurl,
            type: 'post',
            data: {
                action: 'review_logo_description_loading',
                id: id,
                post_id: post_id
            },
            success: function( html ) {
                console.log(html)
                $.magnificPopup.open({
                    removalDelay: 350,
                    mainClass: 'mfp-fade description-popup',
                    items: {
                        src: html,
                        type: 'inline'
                    }
                }, 0);
            }
        });
    });
})(jQuery);